# -*- coding: utf-8 -*-

#必要なライブラリのインポート

import unittest
import HTMLTestRunner
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

# Seleniumユーザーコミュニティのサイトのテストスイート

class SeleniumJpTests(unittest.TestCase):

    # テストスイートのセットアップ

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.implicitly_wait(10)
       
        cls.driver.get("http://www.selenium.jp")

    # ホームページがロードできていることを確認

    def test_01_home(self):
        self.assertTrue(is_element_present(self, By.LINK_TEXT,"Selenium / Appium"))

    # リンクをクリックしてページのロードを確認

    def test_02_click_a_link(self):
        self.driver.find_element_by_link_text("Selenium / Appium").click()
        self.assertTrue(is_element_present(self, By.ID,"sites-page-title"))
 
    # 関連書籍のリンクをクリックし、Selenium本が掲載されていることを確認

    def test_03_click_Books(self):
        self.driver.find_element_by_link_text(u"関連書籍").click()
        self.assertTrue(is_element_present(self, By.LINK_TEXT,u"実践 Selenium WebDriver"))

    # デザインパターン本はまだ掲載されていないので、このテストは失敗する

    def test_04_not_yet_published(self):
        self.driver.find_element_by_link_text(u"関連書籍").click()
        self.assertTrue(is_element_present(self, By.LINK_TEXT,
                                           u"Selenium Design Patterns and Best Practices"))
    
    # 後片付け

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

class OreillyJpTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.implicitly_wait(10)
       
        cls.driver.get("http://www.oreilly.co.jp")

    # Booksのリンクをクリックし、ページのロードを確認
    
    def test_01_click_books(self):
        self.driver.find_element_by_link_text(u"Books").click()
        self.assertTrue(is_element_present(self, By.ID, u"page-title"))

    # Ebookのリンクをクリックし、ページのロードを確認

    def test_02_click_ebooks(self):
        self.driver.find_element_by_link_text(u"Ebook").click()
        self.assertTrue(is_element_present(self, By.ID, u"filter"))
        
    # フィルタリングのフィールドに'Selenium'と入力し、一冊だけ該当することを確認
 
    def test_03_search_seleniumbook(self):
        self.driver.find_element_by_id(u'filter').send_keys('selenium')
        bookTable = self.driver.find_element_by_id(u'bookTable')
        foundBooks = bookTable.find_elements_by_class_name(u'visible')
        self.assertEqual(1, len(foundBooks))
    
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

# 要素があることを確認するためのヘルパーメソッド

def is_element_present(testcase, how, what):
    try:
        testcase.driver.find_element(by=how, value=what)
    except NoSuchElementException, e:
        return False
    return True

# テストの実行
# if __name__ == はPythonの実行ファイルの定型句

if __name__ == '__main__':        

    # テストスイートの読み取り
    seleniumJpTests = unittest.TestLoader().loadTestsFromTestCase(SeleniumJpTests)
    oreillyJpTests = unittest.TestLoader().loadTestsFromTestCase(OreillyJpTests)

    allTests = unittest.TestSuite([seleniumJpTests, oreillyJpTests])

    # 結果のファイルを書き出し用にオープン
    outFile = open('AllTests.html', 'w')

    # テストの実行
    runner = HTMLTestRunner.HTMLTestRunner(stream = outFile, title = 'Test Report', description = 'JaSST15Tokyo demo')
    runner.run(allTests)

    # 結果ファイルのクローズ
    outFile.close()
    

