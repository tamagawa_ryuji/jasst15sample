JaSST 15 Tokyo 配布物の説明

JaSSTSeleniumHandson20150220.ipynb

　ipython notebookで使用するファイルです。



JaSST_SeleniumSampleTest.py

　テストスイートを実行して、結果のレポートをHTMLで生成するサンプルスクリプトです。
　コマンドプロンプト、もしくはターミナルから

　python  JaSST_SeleniumSampleTest.py

　として実行してみてください。


HTMLTestRunner.py

　JaSST_SeleniumSampleTest.pyが使用するライブラリです。
　http://tungwaiyip.info/software/HTMLTestRunner.html　で、BSDライセンスで公開されています。

20150220_jasst_jenkins.pdf

　Jenkinsに関する追加資料です。


pip

　セッション中で、pipコマンドがないというエラーが出た場合、このディレクトリに移動して

　python get-pip.py
　（Macの場合 sudo python get-pip.py）

　としてください。pipコマンドがインストールされます。

